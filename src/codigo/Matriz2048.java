package codigo;

import java.util.Random;
import java.util.ArrayList;

public class Matriz2048 {
	
	private int[][] matriz;
	private final int TAM = 4;
	private final int OBJETIVO = 2048;
	private final int CANT_NUM_INICIALES = 2;
	
	public Matriz2048() {
		
		this.matriz = new int[this.TAM][this.TAM];
		this.inicializarMatriz();
	}
	
	private boolean posEstaVacia(int fila, int columna) {
		
		for (int i = 0; i < this.TAM; i++) {
			if (this.matriz[fila][i] != 0)
				return false;
		}
		return true;
	}
	
	private ArrayList<Integer> posicionAleatoriaVacia() {
		
		int posFila = 0, posColumna = 0;
		boolean posVacia = false;
		while (posVacia == false) {
			posFila = this.generarNumeroDePosicionAleatoria();
			posColumna = this.generarNumeroDePosicionAleatoria();
			posVacia = this.posEstaVacia(posFila, posColumna);
		}
		ArrayList<Integer> pos = new ArrayList<Integer>();
		pos.add(posFila);
		pos.add(posColumna);
		return pos;
	}
	
	private int generarNumeroDePosicionAleatoria() {
		Random aleatorio = new Random(System.currentTimeMillis());
		int pos = aleatorio.nextInt(TAM);
		aleatorio.setSeed(System.currentTimeMillis());
		return pos;
	}
	
	private void inicializarMatriz() {
		
		for (int i = 0; i < this.CANT_NUM_INICIALES; i++ ) {
			ArrayList<Integer> pos = this.posicionAleatoriaVacia();
			this.setPos(pos.get(0), pos.get(1), 2);
		}	
	}
	
	private boolean estaEnMatriz(int numero) {
		for (int fila = 0; fila < this.TAM; fila++) {
			for (int col = 0; col < this.TAM; col++) {
				if (this.matriz[fila][col] == numero)
					return true;
			}
		}
		return false;
	}
	
	public boolean gano() {
		if (estaEnMatriz(this.OBJETIVO)) {
			return true;
		}
		return false;
	}
	
	public boolean perdio() {
		if (estaEnMatriz(0))
			return false;
		return true;
	}
	
	
	//---------------------------------------------------------------------------------------------//
	//---------Movimiento Derecha----------------------//
	
	private int nuevoNumeroAleatorioParaMatriz() {
		int[] arreglo = {2,4};
		Random NumeroAleatorio = new Random(System.currentTimeMillis());
		int numero = NumeroAleatorio.nextInt(2);
		NumeroAleatorio.setSeed(System.currentTimeMillis());
		return arreglo[numero];
	}
	
	private void operarFilaDerecha(int[] fila) {
		
		juntarNumeroDerecha(fila);
		llevarNumeroDerecha(fila);
	}

	private void llevarNumeroDerecha(int[] fila) {
		for (int pos = this.TAM-2; pos >= 0; pos--) {
			if (fila[pos] != 0 ) {
				int posAdelante = pos+1;
				while (posAdelante < this.TAM && fila[posAdelante] == 0) {
					posAdelante++;
				}
				if (fila[posAdelante-1] == 0) {
					fila[posAdelante-1] = fila[pos];
					fila[pos] = 0;
				}
			}
		}
	}

	private void juntarNumeroDerecha(int[] fila) {
		for (int pos = this.TAM-1; pos >= 0; pos-- ) {
			if (fila[pos] != 0) {
				int posAtras = pos-1;
				while (posAtras > 0 && fila[posAtras] == 0 )
					posAtras--;
				if (posAtras >= 0) {
					if (fila[pos] == fila[posAtras]) {
						fila[pos] = fila[pos] + fila[posAtras];
						fila[posAtras] = 0;
					}
				}
			}
		}
	}
	
	private void nuevoNumeroMoverDerecha() {
		if (!perdio() && !gano()) {
			boolean flag = false;
			for (int columna = 0; columna < this.TAM; columna++) {
				for (int fila = 0; fila < this.TAM; fila++) {
					if (matriz[fila][columna] == 0) {
						matriz[fila][columna] = this.nuevoNumeroAleatorioParaMatriz();
						flag = true;
					}
					if (flag)
						break;
				}
				if (flag)
					break;
			}
		}
	}

	/**
	 * 
	 */
	public void moverDerecha() {
		
		if (!this.perdio() && !this.gano()) {
			for (int fila = 0; fila < this.TAM; fila++) {
				this.operarFilaDerecha(this.matriz[fila]);
			}
		}
		//Generamos numero lo mas a la izquierda
		nuevoNumeroMoverDerecha();
	}
	
	//-------------------Moviemieto izquierda------------------//
	
	private void operarFilaIzquierda(int[] fila) {
		
		juntarNumeroIzquierda(fila);
		llevarNumeroIzquierda(fila);
	}
	
	private void llevarNumeroIzquierda(int[] fila) {
		for (int pos = 1; pos < this.TAM; pos++) {
			if (fila[pos] != 0 ) {
				int posAtras = pos-1;
				while (posAtras >= 0 && fila[posAtras] == 0) {
					posAtras--;
				}
				if (fila[posAtras+1] == 0) {
					fila[posAtras+1] = fila[pos];
					fila[pos] = 0;
				}
			}
		}
	}

	private void juntarNumeroIzquierda(int[] fila) {
		for (int pos = 0; pos < this.TAM; pos++ ) {
			if (fila[pos] != 0) {
				int posAdelante = pos+1;
				while (posAdelante < this.TAM && fila[posAdelante] == 0 )
					posAdelante++;
				if (posAdelante < this.TAM) {
					if (fila[pos] == fila[posAdelante]) {
						fila[pos] = fila[pos] + fila[posAdelante];
						fila[posAdelante] = 0;
					}
				}
			}
		}
	}
	
	private void nuevoNumeroMoverIzquierda() {
		if (!perdio() && !gano()) {
			boolean flag = false;
			for (int columna = this.TAM-1; columna >= 0; columna--) {
				for (int fila = 0; fila < this.TAM; fila++) {
					if (matriz[fila][columna] == 0) {
						matriz[fila][columna] = this.nuevoNumeroAleatorioParaMatriz();
						flag = true;
					}
					if (flag)
						break;
				}
				if (flag)
					break;
			}
		}
	}
	
	
	/**
	 * 
	 */
	public void moverIzquierda() {
		if (!this.perdio() && !this.gano()) {
			for (int fila = 0; fila < this.TAM; fila++)
				this.operarFilaIzquierda(this.matriz[fila]);
		}
		
		//generamos nuevo numero lo mas a la derecha
		nuevoNumeroMoverIzquierda();
	}
	
	//-------------------Moviemieto arriba------------------//
	
	/**
	 * 
	 */
	public void moverArriba() {
		if (!this.perdio() && !this.gano()) {
			for (int columna = 0; columna < this.TAM; columna++) {
				this.operarColumnaArriba(columna);
			}
		}
		
		//generamos nuevo numero lo mas abajo posible
		nuevoNumeroMoverArriba();
	}
	
	private void operarColumnaArriba(int columna) {
		juntarNumerosArriba(columna);
		llevarNumerosArriba(columna);
	}

	private void juntarNumerosArriba(int columna) {
		for (int fila = 0; fila < this.TAM; fila++) {
			if (this.matriz[fila][columna] != 0) {
				int filaAbajo = fila+1;
				while (filaAbajo < this.TAM && matriz[filaAbajo][columna] == 0 )
					filaAbajo++;
				if (filaAbajo < this.TAM) {
					if (this.matriz[fila][columna] == this.matriz[filaAbajo][columna]) {
						this.matriz[fila][columna] = matriz[fila][columna] + this.matriz[filaAbajo][columna];
						this.matriz[filaAbajo][columna] = 0;
					}
				}
			}
		}
	}
	
	private void llevarNumerosArriba(int columna) {
		for (int fila = 1; fila < this.TAM; fila++) {
			if (this.matriz[fila][columna] != 0 ) {
				int filaArriba = fila-1;
				while (filaArriba >= 0 && this.matriz[filaArriba][columna] == 0) {
					filaArriba--;
				}
				if (matriz[filaArriba+1][columna] == 0) {
					this.matriz[filaArriba+1][columna] = this.matriz[fila][columna];
					this.matriz[fila][columna] = 0;
				}
			}
		}
	}
	
	private void nuevoNumeroMoverArriba() {
		if (!perdio() && !gano()) {
			boolean flag = false;
			for (int fila = this.TAM-1; fila > 0; fila--) {
				for (int columna = 0; columna < this.TAM; columna++) {
					if (matriz[fila][columna] == 0) {
						matriz[fila][columna] = this.nuevoNumeroAleatorioParaMatriz();
						flag = true;
					}
					if (flag)
						break;
				}
				if (flag)
					break;
			}
		}
	}
	
	//-------------------Moviemieto abajo------------------//
	
	/**
	 * 
	 */
	public void moverAbajo() {
		if (!this.perdio() && !this.gano()) {
			for (int columna = 0; columna < this.TAM; columna++) {
				this.operarColumnaAbajo(columna);
			}
		}
		
		//generamos nuevo numero lo mas arriba posible
		nuevoNumeroMoverAbajo();
	}
	
	private void operarColumnaAbajo(int columna) {
		juntarNumerosAbajo(columna);
		llevarNumerosAbajo(columna);
	}

	private void juntarNumerosAbajo(int columna) {
		for (int fila = this.TAM-1; fila >= 0; fila--) {
			if (this.matriz[fila][columna] != 0) {
				int filaArriba = fila-1;
				while (filaArriba > 0 && matriz[filaArriba][columna] == 0 )
					filaArriba--;
				if (filaArriba >= 0) {
					if (this.matriz[fila][columna] == this.matriz[filaArriba][columna]) {
						this.matriz[fila][columna] = matriz[fila][columna] + this.matriz[filaArriba][columna];
						this.matriz[filaArriba][columna] = 0;
					}
				}
			}
		}
		
	}
	
	private void llevarNumerosAbajo(int columna) {
		for (int fila = this.TAM-2; fila >= 0; fila--) {
			if (this.matriz[fila][columna] != 0 ) {
				int filaAbajo = fila+1;
				while (filaAbajo <= this.TAM-1 && this.matriz[filaAbajo][columna] == 0) {
					filaAbajo++;
				}
				if (matriz[filaAbajo-1][columna] == 0) {
					this.matriz[filaAbajo-1][columna] = this.matriz[fila][columna];
					this.matriz[fila][columna] = 0;
				}
			}
		}
	}
	
	private void nuevoNumeroMoverAbajo() {
		if (!perdio() && !gano()) {
			boolean flag = false;
			for (int fila = 0; fila < this.TAM; fila++) {
				for (int columna = 0; columna < this.TAM; columna++) {
					if (matriz[fila][columna] == 0) {
						matriz[fila][columna] = this.nuevoNumeroAleatorioParaMatriz();
						flag = true;
					}
					if (flag)
						break;
				}
				if (flag)
					break;
			}
		}
	}
	
		
	public void setPos(int fila, int columna, int valor) {
		this.matriz[fila][columna] = valor;
	}
	
	@Override
	public String toString() {
		String ret = "";
		for (int fila = 0; fila < this.TAM; fila++) {
			for (int col = 0; col < this.TAM; col++) {
				ret = ret + (this.matriz[fila][col]) + "        ";
			}
			ret = ret + "\n\n";
		}
		return ret;
	}

}
